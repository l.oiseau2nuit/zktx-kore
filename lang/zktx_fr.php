<?php
// Zktx language file for SPIP --  Fichier langue Zktx pour SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_squelettes_/zktx/lang/

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'dernieres_maj' => 'R&eacute;cement mis &agrave; jour',

	// E
	'erreur_404_titre' => 'Document introuvable !',
	'erreur_404_explication' => 'La page que vous recherchez n\'existe pas, ou a &eacute;t&eacute; d&eacute;plac&eacute;e.',
	'erreur_404_recherche' => 'Que recherchiez vous ?',

	// F
	'forum_desactive' => 'Forum désactivé',
	'forum_desactive_partage' => 'N\'hésitez pas &agrave; relancer la conversation sur vos réseaux sociaux',

	// G
	'go_back_to_top' => 'Retour en haut de page',
	'go_to_content' => 'Acc&eacute;der au contenu',

	// P
	'publie_le' => 'Publié le',
	'publie_dans' => 'sous',
	'pass_erreur_401' => 'Une authentification est n&eacute;cessaire pour acc&eacute;der &agrave; la page.',
	'pass_erreur_503' => 'Service temporairement indisponible ou en maintenance.',

	// R
    'recherche_nouvelle' => 'Effectuer une nouvelle recherche ?',
    'recherche_titre' => 'Votre recherche',
    'recherche_resultats' => 'Voici les r&eacute;sultats :',
    'recherche_no_results_art' => 'Aucun article ne semble correspondre à votre recherche',
    'recherche_no_results_rub' => 'Aucune rubrique ne semble correspondre à votre recherche'
);
